<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>


            <section class="main">
                <div class="container"></div>
            </section>

            <?php include('inc/map.inc.php') ?>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/modal.inc.php') ?>

        <?php include('inc/scripts.inc.php') ?>

    </body>

</html>
